<?xml version="1.0" encoding="UTF-8"?>
<!--
 ! Transforms Markdown input into Bash
 !-->
<!DOCTYPE xsl:transform [
	<!ENTITY NL "&#10;">
	<!ENTITY INDENT "    ">
]>

<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text"/>

<!-- top level document -->
<xsl:template match="/">
	<xsl:text>typeset -A CONF&NL;</xsl:text>
	<xsl:text>CONF=(&NL;</xsl:text>

	<!-- process all top-level definitions lists as one big list -->
	<xsl:apply-templates select="*/dl/dt"/>

	<xsl:text>)&NL;</xsl:text>
</xsl:template>

<!-- items (only one level) -->
<xsl:template match="dt">
	<!-- item name -->
	<xsl:value-of select='concat("&INDENT;[&apos;", normalize-space(.), "&apos;]=")'/>

	<!-- item value -->
	<xsl:apply-templates select="following-sibling::dd[1]"/>
</xsl:template>

<!-- values -->
<xsl:template match="dd">
	<xsl:text>'</xsl:text>
	<xsl:apply-templates/>
	<xsl:text>'&NL;</xsl:text>
</xsl:template>

<!-- new lines in block text -->
<xsl:template match="br">
	<xsl:text>&NL;</xsl:text>
</xsl:template>

<!-- text trimmed -->
<xsl:template match="text()">
	<xsl:value-of select="normalize-space(.)"/>
</xsl:template>

</xsl:transform>
<!--
vim:sw=4:ts=4:ai:fileencoding=utf8:nowrap
-->
