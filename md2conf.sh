#!/bin/sh

# script parameters
OUTPUT_FORMAT=$1
MDCONF=$2

# script name
NAME=${0##*/}
BASE=${0%$NAME}

# syntax message and die
function usage {
	echo 1>&2 "Usage: $NAME <output format> <file>"
	echo 1>&2 "       where output format is 'bash', 'ini' or 'json'."
	exit 2
}

# check format
[[ -n $MDCONF ]] || usage
case $OUTPUT_FORMAT in
	bash|ini|json|xml) ;;
	*) usage ;;
esac


# MD => conf
(	echo '<html>'
	pandoc -f markdown_phpextra "$MDCONF"
	echo '</html>'
) | 
#) | tee debug.html |	# uncomment to inspect pandoc output
case $OUTPUT_FORMAT in
	bash)
		xsltproc ${BASE}md2bash.xsl -
		;;
	ini|xml)
		echo NOT IMPLEMENTED!
		;;
	json)
		xsltproc ${BASE}md2json.xsl -
		;;
esac

# vim:sw=4:ts=4
