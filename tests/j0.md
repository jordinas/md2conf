A
:   scalar

B
:   one line scalar

C
:   list
:   of
:   scalars

D
:   item1
    :   one
    item2
    :   mapping

E
:   item1
    :   list
    item2
    :   of
:   item1
    :   two
    item2
    :   mappings

F
:   list1
    :   1
    :   2
    :   3
    list2
    :   4
    :   5
    :   6

<!--
vim:syntax=markdown:sw=4:ts=4:ai:et:fileencoding=utf8
-->
