<?xml version="1.0" encoding="UTF-8"?>
<!--
 ! Transforms Markdown input into JSON
 !-->
<!DOCTYPE xsl:transform [
	<!ENTITY NL "&#10;">
	<!ENTITY INDENT "    ">
]>

<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text"/>

<!-- top level document -->
<xsl:template match="/">
	<xsl:text>{&NL;</xsl:text>

	<!-- process all top-level definitions lists as one big list -->
	<xsl:apply-templates select="*/dl/dt">
		<xsl:with-param name="indent" select="'&INDENT;'"/>
	</xsl:apply-templates>

	<xsl:text>}&NL;</xsl:text>
</xsl:template>

<!-- items -->
<xsl:template match="dt">
	<xsl:param name="indent"/>

	<!-- item name -->
	<xsl:value-of select="concat($indent, '&quot;', normalize-space(.), '&quot;: ')"/>

	<xsl:variable name="dt" select="."/>
	<xsl:variable name="dds" select="following-sibling::dd[generate-id(preceding-sibling::dt[1]) = generate-id($dt)]"/>

	<!-- item value -->
	<xsl:choose>
		<!-- one dd after dt -->
		<xsl:when test="count($dds) = 1">
			<xsl:apply-templates select="$dds[1]">
				<xsl:with-param name="indent" select="$indent"/>
				<xsl:with-param name="dd_indent" select="''"/>
			</xsl:apply-templates>
		</xsl:when>

		<!-- several dd after dt -->
		<xsl:otherwise>
			<xsl:text>[&NL;</xsl:text>

			<xsl:for-each select="$dds">
				<xsl:apply-templates select="self::dd">
					<xsl:with-param name="indent" select="concat($indent, '&INDENT;')"/>
					<xsl:with-param name="dd_indent" select="concat($indent, '&INDENT;')"/>
				</xsl:apply-templates>

				<xsl:if test="position() != last()">
					<xsl:text>,</xsl:text>
				</xsl:if>
				<xsl:text>&NL;</xsl:text>
			</xsl:for-each>

			<xsl:value-of select="$indent"/>
			<xsl:text>]</xsl:text>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:if test="position() != last()">
		<xsl:text>,</xsl:text>
	</xsl:if>
	<xsl:text>&NL;</xsl:text>
</xsl:template>

<!-- values -->
<xsl:template match="dd">
	<xsl:param name="indent"/>
	<xsl:param name="dd_indent"/>

	<xsl:choose>
		<!-- sublist -->
		<xsl:when test="dl">
			<xsl:value-of select="$dd_indent"/>
			<xsl:text>{&NL;</xsl:text>

			<!-- recurse -->
			<xsl:apply-templates select="dl/dt">
				<xsl:with-param name="indent" select="concat($indent, '&INDENT;')"/>
			</xsl:apply-templates>

			<xsl:value-of select="$indent"/>
			<xsl:text>}</xsl:text>
		</xsl:when>

		<!-- scalar -->
		<xsl:otherwise>
			<xsl:value-of select="concat($dd_indent, '&quot;')"/>
			<xsl:apply-templates/>
			<xsl:value-of select="'&quot;'"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- new lines in block text -->
<xsl:template match="br">
	<xsl:text>\n</xsl:text>
</xsl:template>

<!-- text trimmed -->
<xsl:template match="text()">
	<xsl:value-of select="normalize-space(.)"/>
</xsl:template>

</xsl:transform>
<!--
vim:sw=4:ts=4:ai:fileencoding=utf8:nowrap
-->
